import { Component, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  
  empleados: any[] = [];
  empleado ={
    id:'',
    tipoId: '',
    primerNombre: '',
    otrosNombres: '',
    primerApellido: '',
    segundoApellido: '',
    pais: '',
    email: '',
    fechaIngreso: '',
    fechaRegistro: '',
    area: '',
    estado:'Activo'
  }

  constructor(
    private appService: AppService
  ){

  }

  ngOnInit(): void {
    
    this.getAll();
  }

  getAll(){
    this.appService.getAll()
      .subscribe((data: any) => this.empleados = data);

  }

  create(){
    this.appService.create(this.empleado)
      .subscribe(() => this.getAll());

    this.empleado = {
      id:'',
      tipoId: '',
      primerNombre: '',
      otrosNombres: '',
      primerApellido: '',
      segundoApellido: '',
      pais: '',
      email: '',
      fechaIngreso: '',
      fechaRegistro: '',
      area: '',
      estado:'Activo',
    }
  }

  edit(empleado: any) {

    this.empleado = {
      ...empleado
    };
    
  }

  delete(empleado: any){
    this.appService.delete(empleado.id).subscribe(()=>this.getAll());
  }



}
