import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


const API_BASE = 'http://localhost:8080';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(
    private http: HttpClient
  ) { }


  getAll(){
    return this.http.get(`${API_BASE}/empleados`);
  }

  create(empleado: any){
    return this.http.post(`${API_BASE}/empleados`, empleado);
  }

  update(id:string, empleado: any){
    return this.http.put(`${API_BASE}/empleados/${id}`, empleado);
  }

  delete(id:string){
    return this.http.delete(`${API_BASE}/empleados/${id}`);
  }






}
